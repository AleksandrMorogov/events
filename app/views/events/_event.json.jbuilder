json.extract! event, :id, :name, :description, :event_day, :category_id, :city_id, :created_at, :updated_at
json.url event_url(event, format: :json)
