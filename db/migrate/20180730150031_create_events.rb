class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :event_day
      t.references :category, foreign_key: true
      t.references :city, foreign_key: true

      t.timestamps
    end
  end
end
